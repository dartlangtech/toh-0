## 初始应用程序

欢迎查看 [Dart web应用程序](https://webdev.dartlang.org)的
[示例应用](https://webdev.dartlang.org/angular/tutorial/toh-pt0)页
 .

你可以运行此示例的[托管副本](https://webdev.dartlang.org/examples/toh-0)，也可以创建你自己的本地副本:

1. 创建此示例的本地副本 (使用"克隆 或 下载" 按钮).
2. 获取依赖: `pub get`
3. 启动开发服务器: `pub serve`
4. 在浏览器中打开 [http://localhost:8080](http://localhost:8080)

在Dartium里, 您会立即看到应用程序。 在其他现代浏览器中，您必须稍等一下，pub将转换该应用程序。

---

*注意:* 该仓库的内容是通过运行[dart-doc-syncer](//github.com/dart-lang/dart-doc-syncer)工具从[Angular docs仓库][docs repo] 生成的。 如果您发现此示例代码的问题，请打开一个[issue]。

[docs repo]: //github.com/dart-lang/site-webdev/tree/master/examples/ng/doc/toh-0
[issue]: https://gitee.com/scooplolwiki/toh-0/issues
